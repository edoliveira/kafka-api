$(document).ready(function() {
	$("#salvar").click(function() {

		var pessoa = new Object();
		pessoa.nome = $('#nome').val();
		pessoa.idade = $('#idade').val();

		if (pessoa.nome && pessoa.idade) {
			$.ajax({
				url : 'http://142.93.219.185/api/kafka',
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(pessoa),
				success : function(data, textStatus, xhr) {
					console.log(data);
					alert("Usuário enviado com sucesso!");
				},
				error : function(xhr, textStatus, errorThrown) {
					alert("Erro no consumo do serviço!");
				}
			});
		}
	});
});