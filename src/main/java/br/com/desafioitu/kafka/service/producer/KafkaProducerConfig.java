package br.com.desafioitu.kafka.service.producer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import br.com.desafioitu.kafka.dto.Usuario;

@Configuration
public class KafkaProducerConfig {

	@Bean
	public ProducerFactory<String, Usuario> producerFactory() {

		Map<String, Object> props = new HashMap<String, Object>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka.com:9093");

		props.put("security.protocol", "SASL_SSL");
		props.put("sasl.kerberos.service.name", "kafka");
		props.put("sasl.mechanism", "GSSAPI");

		props.put("ssl.truststore.location", "/home/kafka/seguranca/kafka.client.truststore.jks");
		props.put("ssl.truststore.password", "Oliv123+");
		props.put("ssl.endpoint.identification.algorithm", "");

		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, Usuario.class);

		return new DefaultKafkaProducerFactory<>(props);
	}

	@Bean
	public KafkaTemplate<String, Usuario> kafkaTemplate() {
		return new KafkaTemplate<String, Usuario>(producerFactory());
	}
}